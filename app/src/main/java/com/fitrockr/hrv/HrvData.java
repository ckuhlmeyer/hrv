package com.fitrockr.hrv;


public class HrvData {

    private String id;
    private String userId;
    private long timestampMs;
    private int value;
    private Long offsetInSec;

    public String getId() {
        return id;
    }

    public HrvData setId(String id) {
        this.id = id;
        return this;
    }

    public String getUserId() {
        return userId;
    }

    public HrvData setUserId(String userId) {
        this.userId = userId;
        return this;
    }


    public long getTimestampMs() {
        return timestampMs;
    }

    public HrvData setTimestampMs(long timestampMs) {
        this.timestampMs = timestampMs;
        return this;
    }

    public int getValue() {
        return value;
    }

    public HrvData setValue(int value) {
        this.value = value;
        return this;
    }

    public Long getOffsetInSec() {
        return offsetInSec;
    }

    public HrvData setOffsetInSec(Long offsetInSec) {
        this.offsetInSec = offsetInSec;
        return this;
    }
}
