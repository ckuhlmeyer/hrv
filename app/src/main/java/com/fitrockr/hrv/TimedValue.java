package com.fitrockr.hrv;

public class TimedValue {

    private long time;
    private double value;


    public long getTime() {
        return time;
    }

    public TimedValue setTime(long time) {
        this.time = time;
        return this;
    }

    public double getValue() {
        return value;
    }

    public TimedValue setValue(double value) {
        this.value = value;
        return this;
    }
}
